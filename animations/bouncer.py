# -*- coding: utf-8 -*-

import math
import random

from colors import RAINBOW, RAINBOW_RGB

class Bot(object):
    def __init__(self, size, t0):
        self._size = size
        self._polar_v = None
        self._v = None
        self.set_polar_v([
            random.uniform(0.1, 1),
            random.uniform(0, math.pi * 2.0)
        ])
        self._target_polar_v = None
        self._x = random.randint(0, self._size[0]-1)
        self._y = random.randint(0, self._size[1]-1)

    def set_polar_v(self, polar_v):
        self._polar_v = polar_v
        self._v = [polar_v[0] * math.cos(polar_v[1]), 
                  polar_v[0] * math.sin(polar_v[1])]

    def pos(self, t):
        if self._target_polar_v is None:
            if random.randint(0, 25) == 0:
                # Randomly pick a new speed.
                self._target_polar_v = [
                    random.uniform(0.1, 1),
                    # Don't do a U-turn
                    self._v[1] + random.uniform(- 3 * math.pi / 4, 3 * math.pi / 4)
                ]
        else:
            # Progressively steer towards the new speed.
            diff = [
                (self._target_polar_v[0] - self._polar_v[0]) / 10.0,
                (self._target_polar_v[1] - self._polar_v[1]) / 10.0
            ]
            if abs(diff[0]) < 0.001 and abs(diff[1]) < 0.001:
                self.set_polar_v(self._target_polar_v)
                self._target_polar_v = None
            else:
                self.set_polar_v([
                    self._polar_v[0] + diff[0],
                    self._polar_v[1] + diff[1]
                ])

        self._x += self._v[0]
        if self._x <= 0:
            self._x = -self._x
            self.set_polar_v([self._polar_v[0], math.pi - self._polar_v[1]])
            self._target_polar_v = None
        if self._x >= self._size[0]:
            self._x = 2 * self._size[0] - self._x
            self.set_polar_v([self._polar_v[0], math.pi - self._polar_v[1]])
            self._target_polar_v = None

        self._y += self._v[1]
        if self._y <= 0:
            self._y = -self._y
            self.set_polar_v([self._polar_v[0], -self._polar_v[1]])
            self._target_polar_v = None
        if self._y >= self._size[1]:
            self._y = 2 * self._size[1] - self._y
            self.set_polar_v([self._polar_v[0], -self._polar_v[1]])
            self._target_polar_v = None

        return (self._x, self._y)


class BouncerAnimation(object):
    def __init__(self, bots, duration):
        self._bots = bots
        self._duration = duration

    def animate(self, animator, img, draw):
        size = img.size

        bots = [
            Bot(size, animator.t)
            for b in range(self._bots)
        ]

        while self._duration == 0 or animator.t < self._duration:
            # Fade screen
            animator.fade(0.99)

            # Draw the bots
            for i, bot in enumerate(bots):
                color = RAINBOW[int(63 * i + 32 * animator.t)%len(RAINBOW)]
                draw.point(bot.pos(animator.t), fill=color)

            yield

def build_animation(configuration):
    return BouncerAnimation(
        bots = configuration.bots.value(25),
        duration = configuration.duration.value(0)
    )
