# -*- coding: utf-8 -*-

import time
import traceback
import json

from configuration import Configuration
import animator

if __name__ == '__main__':
    # Load configuration
    configuration = Configuration.load('configuration.json')

    # Try to connect to LED display through serial port
    display = None
    try:
        import leddisplay
        display = leddisplay.Display(
            port = configuration.leddisplay.port.required().encode()
        )
    except leddisplay.serial.SerialException, e:
        print "Could not connect to serial port, launching display emulator"
        print "\t%s"%e
    except:
        traceback.print_exc()

    # If connection to LED display was not successfull,
    # launch the emulator
    if display is None:
        import tkdisplay
        display = tkdisplay.Display(
            (
                configuration.tkdisplay.width.value(32),
                configuration.tkdisplay.height.value(16)
            ),
            configuration.tkdisplay.scale.value(4)
        )

    # Create the animator
    animator = animator.Animator(
        display,
        configuration.animator
    )
    animator.start()
    
    try:
        display.mainloop()
    finally:
        display.close()