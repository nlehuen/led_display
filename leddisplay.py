# -*- coding: utf-8 -*-

import serial
import traceback
from Queue import Queue, Full

class Display(object):
    def __init__(self, port):
        self._serial = serial.Serial(port, 2000000, timeout=1)
        self._queue = Queue(8)
        self._keepgoing = True

    def size(self):
        return (32, 16)

    def send_image(self, img):
        # Crop source image if needed
        if img.size != (32, 16):
            img = img.crop((0, 0, 32, 16))

        # Encoding is performed in the caller (i.e. the animator) thread
        # so that the image can be reused for the next frame
        data = '\x02\x01' + img.tobytes("raw", "RGB").replace('\x02','\x03')

        while True:
            try:
                self._queue.put(data, block=False)
                return
            except Full:
                # Silently drops images if the screen cannot keep up
                self._queue.get()

    def close(self):
        self._keepgoing = False

    def mainloop(self):
        while self._keepgoing:
            data = self._queue.get()
            self._serial.write(data)
        self._serial.close()
